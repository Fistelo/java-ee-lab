package com.fistelo.pg.service;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Collection;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

import com.fistelo.pg.persistence.dao.TransferJpaDao;
import com.fistelo.pg.persistence.entity.Transfer;

@ApplicationScoped
public class TransferService implements Serializable {

  @Inject
  private TransferJpaDao transferJpaDao;

  public Collection<Transfer> findAllTransfers() {
    return transferJpaDao.findAll();
  }

  public Transfer findTransfer(int id) {
    return transferJpaDao.find(id);
  }

  @Transactional
  public void removeTransfer(Transfer transfer) {
    transferJpaDao.remove(transfer);
  }

  //do smth with that if
  @Transactional
  public void saveTransfer(Transfer transfer) {
    if (!transfer.getDate().isAfter(LocalDate.now())) {
    }
    transferJpaDao.add(transfer);
  }

  @Transactional
  public void updateTransfer(Transfer transfer) {
    transferJpaDao.update(transfer);
  }
}
