package com.fistelo.pg.service;

import static java.math.BigDecimal.ZERO;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

import com.fistelo.pg.persistence.dao.AccountJpaDao;
import com.fistelo.pg.persistence.entity.Account;
import com.fistelo.pg.persistence.entity.Transfer;

@ApplicationScoped
public class AccountService implements Serializable {

  public static final int MINIMAL_BALANCE = 0;

  @Inject
  private AccountJpaDao accountJpaDao;

  public Collection<Account> findAllAccounts() {
    return accountJpaDao.findAll();
  }

  public Account findAccount(int id) {
    return accountJpaDao.find(id);
  }

  @Transactional
  public void removeAccount(Account account) {
    accountJpaDao.remove(account);
  }

  @Transactional
  public void saveAccount(Account account) {
    if (account.getBalance().compareTo(sumTransfers(account)) >= MINIMAL_BALANCE) {
      accountJpaDao.add(account);
    }
  }

  //duplicated code
  @Transactional
  public void updateAccount(Account account) {
    if (account.getBalance().compareTo(sumTransfers(account)) >= MINIMAL_BALANCE) {
      accountJpaDao.update(account);
    }
  }

  //Stream?
  private BigDecimal sumTransfers(final Account account) {
    BigDecimal sum = ZERO;
    for (Transfer transfer : account.getTransfers()) {
      sum = sum.add(transfer.getValue());
    }
    return sum;
  }
}
