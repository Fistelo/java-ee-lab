package com.fistelo.pg.service;

import java.io.Serializable;
import java.util.Collection;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

import com.fistelo.pg.persistence.dao.CardJpaDao;
import com.fistelo.pg.persistence.entity.Card;

@ApplicationScoped
public class CardService implements Serializable {

  @Inject
  private CardJpaDao cardJpaDao;

  public Collection<Card> findAllCards() {
    return cardJpaDao.findAll();
  }

  public Card findCard(int id) {
    return cardJpaDao.find(id);
  }

  @Transactional
  public void removeCard(Card card) {
    cardJpaDao.remove(card);
  }

  @Transactional
  public void saveCard(Card card) {
    cardJpaDao.add(card);
  }

  @Transactional
  public void update(Card card) {
    cardJpaDao.update(card);
  }
}
