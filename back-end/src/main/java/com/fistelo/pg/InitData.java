package com.fistelo.pg;

import static java.util.Arrays.asList;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Initialized;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import com.fistelo.pg.persistence.dao.AccountJpaDao;
import com.fistelo.pg.persistence.dao.CardJpaDao;
import com.fistelo.pg.persistence.dao.TransferJpaDao;
import com.fistelo.pg.persistence.entity.Account;
import com.fistelo.pg.persistence.entity.Card;
import com.fistelo.pg.persistence.entity.Transfer;

@ApplicationScoped
public class InitData {
  @Inject
  private CardJpaDao cardJpaDao;
  @Inject
  private AccountJpaDao accountJpaDao;
  @Inject
  private TransferJpaDao transferJpaDao;

  @Transactional
  public void init(@Observes @Initialized(ApplicationScoped.class) Object init) {
    final Transfer transfer1 = new Transfer("Oddaje dzieki", BigDecimal.valueOf(10L), LocalDate.of(2031, 3, 12), "213124234242332");
    final Transfer transfer2 = new Transfer("Za bilet2", BigDecimal.valueOf(30L), LocalDate.of(2020, 10, 2), "432423342342342");
    final Transfer transfer3 = new Transfer("Za bilet3", BigDecimal.valueOf(30L), LocalDate.of(2020, 10, 2), "432423342342342");
    final Transfer transfer4 = new Transfer("Za bilet4", BigDecimal.valueOf(30L), LocalDate.of(2020, 10, 2), "432423342342342");
    final Transfer transfer5 = new Transfer("Za bilet5", BigDecimal.valueOf(30L), LocalDate.of(2020, 10, 2), "432423342342342");
    final Transfer transfer6 = new Transfer("Za bilet6", BigDecimal.valueOf(30L), LocalDate.of(2020, 10, 2), "432423342342342");
    final Transfer transfer7 = new Transfer("Za bilet7", BigDecimal.valueOf(30L), LocalDate.of(2020, 10, 2), "432423342342342");
    final Transfer transfer8 = new Transfer("Za bilet8", BigDecimal.valueOf(30L), LocalDate.of(2020, 10, 2), "432423342342342");
    final Transfer transfer9 = new Transfer("Za bilet9", BigDecimal.valueOf(30L), LocalDate.of(2020, 10, 2), "432423342342342");
    final Transfer transfer10 = new Transfer("Za bilet10", BigDecimal.valueOf(30L), LocalDate.of(2020, 10, 2), "432423342342342");

    final Account account1 = new Account("Prywatne", "8927345987937645", BigDecimal.valueOf(54768L));
    final Account account2 = new Account("Firmowe", "1233214325533", BigDecimal.valueOf(100L));

    final Card card1 = new Card("Karta 1", "1234123412341234", "123");
    final Card card2 = new Card("Karta 2", "7654435285930284", "321");

    account1.addTransferMutually(transfer1);
    account1.addTransferMutually(transfer2);
    account1.addTransferMutually(transfer3);

    account2.addTransferMutually(transfer4);
    account2.addTransferMutually(transfer5);
    account2.addTransferMutually(transfer6);
    account2.addTransferMutually(transfer7);
    account2.addTransferMutually(transfer8);
    account2.addTransferMutually(transfer9);

    account1.addCardMutually(card1);
    account2.addCardMutually(card2);

    cardJpaDao.addAll(asList(card1, card2));
    transferJpaDao.addAll(asList(transfer1, transfer2, transfer3, transfer4, transfer5, transfer6, transfer7, transfer8, transfer9, transfer10));
    accountJpaDao.addAll(asList(account1, account2));

    List<Card> cards = cardJpaDao.findAll();
    List<Transfer> trs = transferJpaDao.findAll();
    List<Account> accs = accountJpaDao.findAll();

    System.out.println();
  }
}

