package com.fistelo.pg.api.controller;

import static com.fistelo.pg.api.tools.UriUtils.uri;
import static javax.ws.rs.core.Response.created;
import static javax.ws.rs.core.Response.noContent;
import static javax.ws.rs.core.Response.ok;
import static javax.ws.rs.core.Response.status;

import java.util.Collection;

import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import com.fistelo.pg.persistence.entity.Card;
import com.fistelo.pg.service.CardService;

@Path("/cards")
public class CardController {

  @Inject
  CardService cardService;

  @GET
  @Path("/{card}")
  public Card getCard(@PathParam("card") Card card) {
    return card;
  }

  @GET
  public Collection<Card> getAllCards() {
    return cardService.findAllCards();
  }

  @POST
  public Response saveCard(Card card) {
    if(isCardEmpty(card)) {
      return status(Response.Status.BAD_REQUEST).build();
    }
    cardService.saveCard(card);
    return created(uri(CardController.class, "getCard", card.getId())).build();
  }

  @DELETE
  @Path("/{card}")
  public Response deleteCard(@PathParam("card") Card card) {
    cardService.removeCard(card);
    return noContent().build();
  }

  @PUT
  @Path("/{card}")
  public Response updateCard(@PathParam("card") Card originalCard, Card updatedCard) {
    if (!originalCard.getId().equals(updatedCard.getId()) || isCardEmpty(updatedCard)) {
      return status(Response.Status.BAD_REQUEST).build();
    }

    cardService.update(updatedCard);
    return ok().build();
  }

  private boolean isCardEmpty(final Card card) {
    return card.getName().isEmpty() && card.getNumber().isEmpty();
  }
}
