package com.fistelo.pg.api.converter;

import com.fistelo.pg.service.AccountService;
import com.fistelo.pg.persistence.entity.Account;

import javax.ws.rs.ext.Provider;

@Provider
public class AccountConverter extends AbstractEntityConverter<Account> {
    public AccountConverter() {
        super(Account.class, Account::getId, AccountService::findAccount);
    }
}
