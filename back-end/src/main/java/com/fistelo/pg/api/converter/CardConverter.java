package com.fistelo.pg.api.converter;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.inject.Inject;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.ext.ParamConverter;
import javax.ws.rs.ext.ParamConverterProvider;
import javax.ws.rs.ext.Provider;

import com.fistelo.pg.persistence.entity.Card;
import com.fistelo.pg.service.CardService;

@Provider
public class CardConverter implements ParamConverterProvider {
  @Inject
  CardService cardService;

  @Override
  public <T> ParamConverter<T> getConverter(Class<T> rawType, Type genericType, Annotation[] annotations) {
    if (rawType != Card.class) {
      return null;
    }

    return (ParamConverter<T>) new ParamConverter<Card>() {
      @Override
      public Card fromString(String value) {
        Card card = cardService.findCard(Integer.valueOf(value));

        if (card == null) {
          throw new NotFoundException();
        }

        return card;
      }

      @Override
      public String toString(Card card) {
        return card.getId().toString();
      }
    };
  }
}
