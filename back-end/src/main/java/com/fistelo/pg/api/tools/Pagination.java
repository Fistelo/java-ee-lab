package com.fistelo.pg.api.tools;

import java.util.stream.Stream;

import javax.ws.rs.core.Link;
import javax.ws.rs.core.UriInfo;

public class Pagination {
  public static final String PREVIOUS_PAGE = "prev";
  public static final String NEXT_PAGE = "next";
  public static final String FIRST_PAGE = "first";
  public static final String LAST_PAGE = "last";
  public static final String PAGE_QUERY_PARAM = "page";
  public static final int FIRST_PAGE_NUMBER = 1;

  public final int pageCount;
  public final int currentPageIndex;

  public Pagination(int currentPageIndex, int pageCount) {
    this.currentPageIndex = currentPageIndex;
    this.pageCount = pageCount;
  }

  public Stream<Link> toLinks(UriInfo uriInfo) {
    if (currentPageIndex == FIRST_PAGE_NUMBER && pageCount == 1) {
      return Stream.empty();
    }

    Stream.Builder<Link> linkStreamBuilder = Stream.builder();

    if (currentPageIndex > FIRST_PAGE_NUMBER) {
      linkStreamBuilder.accept(
          Link.fromUriBuilder(uriInfo.getRequestUriBuilder()
              .replaceQueryParam(PAGE_QUERY_PARAM, currentPageIndex - 1))
              .rel(PREVIOUS_PAGE)
              .build());
    }

    if (currentPageIndex < pageCount) {
      linkStreamBuilder.accept(
          Link.fromUriBuilder(uriInfo.getRequestUriBuilder()
              .replaceQueryParam(PAGE_QUERY_PARAM, currentPageIndex + 1))
              .rel(NEXT_PAGE)
              .build());
    }

    linkStreamBuilder.accept(
        Link.fromUriBuilder(uriInfo.getRequestUriBuilder()
            .replaceQueryParam(PAGE_QUERY_PARAM, FIRST_PAGE_NUMBER))
            .rel(FIRST_PAGE)
            .build());

    linkStreamBuilder.accept(
        Link.fromUriBuilder(uriInfo.getRequestUriBuilder()
            .replaceQueryParam(PAGE_QUERY_PARAM, pageCount))
            .rel(LAST_PAGE)
            .build());

    return linkStreamBuilder.build();
  }
}