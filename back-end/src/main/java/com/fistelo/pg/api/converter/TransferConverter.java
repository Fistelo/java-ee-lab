package com.fistelo.pg.api.converter;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import com.fistelo.pg.service.AccountService;
import com.fistelo.pg.persistence.entity.Transfer;
import com.fistelo.pg.service.TransferService;

import javax.inject.Inject;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.ext.ParamConverter;
import javax.ws.rs.ext.ParamConverterProvider;
import javax.ws.rs.ext.Provider;

@Provider
public class TransferConverter implements ParamConverterProvider {

    @Inject
    TransferService transferService;

    @Override
    public <T> ParamConverter<T> getConverter(Class<T> rawType, Type genericType, Annotation[] annotations) {
        if (rawType != Transfer.class) {
            return null;
        }

        return (ParamConverter<T>) new ParamConverter<Transfer>() {
            @Override
            public Transfer fromString(String value) {
                Transfer entity = transferService.findTransfer(Integer.valueOf(value));

                if (entity == null) {
                    throw new NotFoundException();
                }

                return entity;
            }

            @Override
            public String toString(Transfer transfer) {
                return transfer.getId().toString();
            }
        };
    }
}
