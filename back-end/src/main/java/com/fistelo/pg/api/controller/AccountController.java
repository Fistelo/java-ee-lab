package com.fistelo.pg.api.controller;

import com.fistelo.pg.service.AccountService;
import com.fistelo.pg.persistence.entity.Account;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import java.util.Collection;

import static com.fistelo.pg.api.tools.UriUtils.uri;
import static javax.ws.rs.core.Response.*;

@Path("/accounts")
public class AccountController {

  @Inject
  AccountService accountService;

  @GET
  public Collection<Account> getAllAccounts() {
    return accountService.findAllAccounts();
  }

  @POST
  public Response saveAccount(Account account) {
    accountService.saveAccount(account);
    return created(uri(AccountController.class, "getAccount", account.getId())).build();
  }

  @GET
  @Path("/{account}")
  public Account getAccount(@PathParam("account") Account account) {
    return account;
  }

  @DELETE
  @Path("/{account}")
  public Response deleteAccount(@PathParam("account") Account account) {
    accountService.removeAccount(account);
    return noContent().build();
  }

  //TODO co z tym ifem wtf
  @PUT
  @Path("/{account}")
  public Response updateAccount(@PathParam("account") Account originalAccount, Account updatedAccount) {
    if (!originalAccount.getId().equals(updatedAccount.getId())) {
      return status(Status.BAD_REQUEST).build();
    }

    accountService.updateAccount(updatedAccount);
    return ok().build();
  }
}
