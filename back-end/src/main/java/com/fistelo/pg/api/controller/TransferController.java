package com.fistelo.pg.api.controller;

import static com.fistelo.pg.api.tools.UriUtils.uri;
import static javax.ws.rs.core.Response.created;
import static javax.ws.rs.core.Response.noContent;
import static javax.ws.rs.core.Response.ok;
import static javax.ws.rs.core.Response.status;

import java.util.Collection;

import com.fistelo.pg.persistence.entity.Transfer;
import com.fistelo.pg.service.TransferService;

import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

@Path("/transfers")
public class TransferController {

  @Inject
  TransferService transferService;
  @Context
  UriInfo uriInfo;

  @GET
  public Collection<Transfer> getAllTransfers() {
    return transferService.findAllTransfers();
  }

  @GET
  @Path("/{transfer}")
  public Transfer getTransfer(@PathParam("transfer") Transfer transfer) {
    return transfer;
  }

  @POST
  public Response saveTransfer(Transfer transfer) {
    transferService.saveTransfer(transfer);
    return created(uri(TransferController.class, "getTransfer", transfer.getId())).build();
  }

  @DELETE
  @Path("/{transfer}")
  public Response deleteTransfer(@PathParam("transfer") Transfer transfer) {
    transferService.removeTransfer(transfer);
    return noContent().build();
  }

  @PUT
  @Path("/{transfer}")
  public Response updateTransfer(@PathParam("transfer") Transfer original, Transfer updated) {
    if (!original.getId().equals(updated.getId())) {
      return status(Response.Status.BAD_REQUEST).build();
    }

    transferService.updateTransfer(updated);
    return ok().build();
  }
}
