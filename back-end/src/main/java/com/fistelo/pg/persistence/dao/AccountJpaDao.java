package com.fistelo.pg.persistence.dao;

import javax.enterprise.context.ApplicationScoped;

import com.fistelo.pg.persistence.entity.Account;

@ApplicationScoped
public class AccountJpaDao extends JpaDao<Account>{
  public AccountJpaDao(){
    super(Account.class);
  }
}
