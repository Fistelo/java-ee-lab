package com.fistelo.pg.persistence.dao;

import javax.enterprise.context.ApplicationScoped;

import com.fistelo.pg.persistence.entity.Card;

@ApplicationScoped
public class CardJpaDao extends JpaDao<Card> {
  public CardJpaDao() {
    super(Card.class);
  }
}
