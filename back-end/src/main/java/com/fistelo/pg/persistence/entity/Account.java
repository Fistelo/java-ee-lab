package com.fistelo.pg.persistence.entity;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.REFRESH;
import static javax.persistence.CascadeType.REMOVE;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@EqualsAndHashCode(of = "id")
@NoArgsConstructor
@Getter
@Setter
public class Account implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  private String name;

  private String number;

  private BigDecimal balance;

  @OneToMany(mappedBy = "account", cascade = REMOVE)
  private List<Transfer> transfers = new ArrayList<>();

  @OneToOne(mappedBy = "account", cascade = REMOVE)
  private Card card;

  public Account(final String name, final String number, final BigDecimal balance) {
    this.name = name;
    this.number = number;
    this.balance = balance;
  }

  public void addTransferMutually(Transfer transfer) {
    transfer.setAccount(this);
    this.transfers.add(transfer);
  }

  public void addCardMutually(Card card) {
    card.setAccount(this);
    this.card = card;
  }
}
