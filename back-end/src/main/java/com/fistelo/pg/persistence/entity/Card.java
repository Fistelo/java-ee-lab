package com.fistelo.pg.persistence.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@EqualsAndHashCode(of = "id")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@NamedQueries({
    @NamedQuery(name = Card.Queries.FIND_ALL, query = "select a from Card a")
})
public class Card implements Serializable {

  public static class Queries {
    public static final String FIND_ALL = "CARD_FIND_ALL";
  }

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  private String name;

  private String number;

  private String cvv;

  @OneToOne
  @JsonIgnore
  private Account account;

  public Card(final String name, final String number, final String cvv) {
    this.name = name;
    this.number = number;
    this.cvv = cvv;
  }
}
