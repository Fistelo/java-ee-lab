package com.fistelo.pg.persistence.dao;

import javax.enterprise.context.ApplicationScoped;

import com.fistelo.pg.persistence.entity.Transfer;

@ApplicationScoped
public class TransferJpaDao extends JpaDao<Transfer>{
  public TransferJpaDao(){
    super(Transfer.class);
  }
}
