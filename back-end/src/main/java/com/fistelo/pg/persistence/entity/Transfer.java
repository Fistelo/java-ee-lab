package com.fistelo.pg.persistence.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fistelo.pg.api.tools.LocalDateAdapter;
import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@Entity
@EqualsAndHashCode(of = "id")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Transfer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String title;

    private BigDecimal value;

    private LocalDate date;

    private String destinationAccountNumber;

    @ManyToOne
    @JsonIgnore
    private Account account;

    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public LocalDate getDate (){
        return date;
    }

    public Transfer(final String title, final BigDecimal value, final LocalDate date, final String destinationAccountNumber) {
        this.title = title;
        this.value = value;
        this.date = date;
        this.destinationAccountNumber = destinationAccountNumber;
    }
}
