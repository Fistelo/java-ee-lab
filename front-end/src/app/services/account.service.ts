import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Account} from '../model/account';
import {Transfer} from '../model/transfer';
import {Card} from "../model/card";

@Injectable()
export class AccountService {

  constructor(private http: HttpClient) {
  }

  findAllAccounts(): Observable<Account[]> {
    return this.http.get<Account[]>('api/accounts');
  }

  findAccount(id: number): Observable<Account> {
    return this.http.get<Account>(`api/accounts/${id}`);
  }

  removeAccount(account: Account): Observable<any> {
    return this.http.delete(`api/accounts/${account.id}`);
  }

  saveAccount(account: Account): Observable<any> {
    if (account.id) {
      return this.http.put(`api/accounts/${account.id}`, account);
    } else {
      return this.http.post('api/accounts/', account);
    }
  }


  findAllTransfers(): Observable<Transfer[]> {
    return this.http.get<Transfer[]>('api/transfers');
  }

  findTransfer(id: number): Observable<Transfer> {
    return this.http.get<Transfer>(`api/transfers/${id}`);
  }

  saveTransfer(transfer: Transfer): Observable<any> {
    if (transfer.id) {
      return this.http.put(`api/transfers/${transfer.id}`, transfer);
    } else {
      return this.http.post('api/transfers/', transfer);
    }
  }

  removeTransfer(transfer: Transfer): Observable<any> {
    return this.http.delete(`api/transfers/${transfer.id}`);
  }


  findAllCards(): Observable<Card[]> {
    return this.http.get<Card[]>('api/cards');
  }

  findCard(id: number): Observable<Card> {
    return this.http.get<Card>(`api/cards/${id}`);
  }

  saveCard(card: Card): Observable<any> {
    if (card.id) {
      return this.http.put(`api/cards/${card.id}`, card);
    } else {
      return this.http.post('api/cards/', card);
    }
  }

  removeCard(card: Card): Observable<any> {
    return this.http.delete(`api/cards/${card.id}`);
  }
}
