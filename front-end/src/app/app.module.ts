import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './components/app/app.component';
import {AppRoutingModule} from './app-routing.module';
import {ListAccountsComponent} from './components/list-accounts/list-accounts.component';
import {HttpClientModule} from '@angular/common/http';
import {AccountService} from './services/account.service';
import {ListTransfersComponent} from './components/list-transfers/list-transfers.component';
import {EditAccountComponent} from './components/edit-account/edit-account.component';
import {FormsModule} from '@angular/forms';
import {EditTransferComponent} from "./components/edit-transfer/edit-transfer.component";
import {ListCardsComponent} from "./components/list-cards/list-cards.component";
import {EditCardComponent} from "./components/edit-card/edit-card.component";


@NgModule({
  declarations: [
    AppComponent,
    ListAccountsComponent,
    ListTransfersComponent,
    ListCardsComponent,
    EditAccountComponent,
    EditTransferComponent,
    EditCardComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,

    AppRoutingModule
  ],
  providers: [AccountService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
