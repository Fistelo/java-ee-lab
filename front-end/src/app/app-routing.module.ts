import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ListAccountsComponent} from './components/list-accounts/list-accounts.component';
import {ListTransfersComponent} from './components/list-transfers/list-transfers.component';
import {EditAccountComponent} from './components/edit-account/edit-account.component';
import {EditTransferComponent} from "./components/edit-transfer/edit-transfer.component";
import {ListCardsComponent} from "./components/list-cards/list-cards.component";
import {EditCardComponent} from "./components/edit-card/edit-card.component";

const routes: Routes = [
  {path: '', component: ListAccountsComponent},
  {path: 'accounts/new', component: EditAccountComponent},
  {path: 'accounts/:id/edit', component: EditAccountComponent},
  {path: 'transfers/new', component: EditTransferComponent},
  {path: 'transfers/:id/edittransfer', component: EditTransferComponent},
  {path: 'transfers', component: ListTransfersComponent},
  {path: 'cards', component: ListCardsComponent},
  {path: 'cards/new', component: EditCardComponent},
  {path: 'cards/:id/editcard', component: EditCardComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
