import {Component, OnInit} from '@angular/core';
import {AccountService} from '../../services/account.service';
import {Observable} from 'rxjs/Observable';
import {Account} from '../../model/account';

@Component({
  selector: 'app-list-accounts',
  templateUrl: './list-accounts.component.html'
})
export class ListAccountsComponent implements OnInit {

  accounts: Observable<Account[]>;

  constructor(private accountService: AccountService) {
  }

  ngOnInit() {
    this.accounts = this.accountService.findAllAccounts();
  }

  remove(account: Account) {
    this.accountService.removeAccount(account)
      .subscribe(() => this.ngOnInit());
  }
}
