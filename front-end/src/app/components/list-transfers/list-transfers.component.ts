import { Component, OnInit } from '@angular/core';
import {AccountService} from '../../services/account.service';
import {Transfer} from '../../model/transfer';
import {Observable} from 'rxjs/Observable';
import {Account} from "../../model/account";

@Component({
  selector: 'app-list-transfers',
  templateUrl: './list-transfers.component.html'
})
export class ListTransfersComponent implements OnInit {

  transfers: Observable<Transfer[]>;

  constructor(private accountService: AccountService) { }

  ngOnInit() {
    this.transfers = this.accountService.findAllTransfers();
  }

  remove(transfer: Transfer) {
    this.accountService.removeTransfer(transfer)
      .subscribe(() => this.ngOnInit());
  }
}
