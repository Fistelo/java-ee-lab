import {Component, OnInit} from '@angular/core';
import {Account} from '../../model/account';
import {AccountService} from '../../services/account.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Transfer} from '../../model/transfer';
import {Card} from "../../model/card";

@Component({
  selector: 'app-edit-account',
  templateUrl: './edit-account.component.html'
})
export class EditAccountComponent implements OnInit {

  account: Account;
  availableTransfers: Transfer[];
  availableCards: Card[];

  constructor(private accountsService: AccountService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');

    if (id == null) {
      this.account = {id: null, name: '', number: '', balance: null, card: null, transfers: []};
    } else {
      this.accountsService.findAccount(Number(id))
        .subscribe(account => this.account = account);
    }

    this.accountsService.findAllTransfers()
      .subscribe(transfers => this.availableTransfers = transfers);

    this.accountsService.findAllCards()
      .subscribe(cards => this.availableCards = cards);
  }

  save() {
    this.accountsService.saveAccount(this.account)
      .subscribe(() => this.router.navigateByUrl(''));
  }

  compareTransfers(transfer1: Transfer, transfer2: Transfer): boolean {
    return transfer1 && transfer2 ? transfer1.id === transfer2.id : transfer1 === transfer2;
  }

  compareCards(card1: Card, card2: Card): boolean{
    return card1 && card2 ? card1.id === card2.id : card1 === card2;
  }
}
