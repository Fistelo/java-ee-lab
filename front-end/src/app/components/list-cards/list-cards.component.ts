import {Component, OnInit} from '@angular/core';
import {AccountService} from '../../services/account.service';
import {Observable} from 'rxjs/Observable';
import {Card} from "../../model/card";

@Component({
  selector: 'app-list-cards',
  templateUrl: './list-cards.component.html'
})
export class ListCardsComponent implements OnInit {

  cards: Observable<Card[]>;

  constructor(private accountService: AccountService) { }

  ngOnInit() {
    this.cards = this.accountService.findAllCards();
  }

  remove(card: Card) {
    this.accountService.removeCard(card)
      .subscribe(() => this.ngOnInit());
  }
}
