import {Component, OnInit} from '@angular/core';
import {AccountService} from '../../services/account.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Transfer} from '../../model/transfer';

@Component({
  selector: 'app-edit-transfer',
  templateUrl: './edit-transfer.component.html'
})
export class EditTransferComponent implements OnInit {

  transfer: Transfer;

  constructor(private accountsService: AccountService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');

    if (id == null) {
      this.transfer = {id: null, title: '', value: null, date: '', destinationAccountNumber: ""};
    } else {
      this.accountsService.findTransfer(Number(id))
        .subscribe(transfer => this.transfer = transfer);
    }
  }

  save() {
    this.accountsService.saveTransfer(this.transfer)
      .subscribe(() => this.router.navigateByUrl('transfers'));
  }
}
