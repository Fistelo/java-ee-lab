import {Component, OnInit} from '@angular/core';
import {Account} from '../../model/account';
import {AccountService} from '../../services/account.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Transfer} from '../../model/transfer';
import {Card} from "../../model/card";

@Component({
  selector: 'app-edit-card',
  templateUrl: './edit-card.component.html'
})
export class EditCardComponent implements OnInit {

  card: Card;

  constructor(private accountsService: AccountService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');

    if (id == null) {
      this.card = {id: null, name: '', number: '', cvv: ''};
    } else {
      this.accountsService.findCard(Number(id))
        .subscribe(card => this.card = card);
    }
  }

  save() {
    this.accountsService.saveCard(this.card)
      .subscribe(() => this.router.navigateByUrl('cards'));
  }
}
