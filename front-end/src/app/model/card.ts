export class Card {
  id: number;
  name: string;
  number: string;
  cvv: string;
}
