export class Transfer {
  id: number;
  title: string;
  value: number;
  date: string;
  destinationAccountNumber: string;
}
