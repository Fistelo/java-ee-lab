import {Transfer} from './transfer';
import {Card} from './card';

export class Account {
  id: number;
  name: string;
  number: string;
  balance: number;
  card: Card;
  transfers: Transfer[];
}
